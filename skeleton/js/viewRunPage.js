// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    var runNames = [ "Run A", "Run B" ];
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}

let current =[145.1329004, -37.9118667];
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 18,
    center: current
}); 


var inx = localStorage.getItem('index')
var dataTaken = JSON.parse(localStorage.getItem('runList'))
function deleteRun() {
    dataTaken.splice(inx,1)
    localStorage.setItem('runList',JSON.stringify(dataTaken))
}

document.getElementById("headerBarTitle").innerHTML = dataTaken[inx].name;


var dateRun = dataTaken[inx].date
document.getElementById('date').innerHTML += dateRun

var DisTraveled = dataTaken[inx].distanceT.toFixed(3);
document.getElementById('distanceTravelled').innerHTML += DisTraveled + 'm';

var runTime = dataTaken[inx].time.toFixed(3);
document.getElementById('c').innerHTML += runTime + 's'

var avgSpeedRun = dataTaken[inx].avgSpeedRun.toFixed(3);
document.getElementById('avgSpeed').innerHTML += avgSpeedRun+'m/s'

var startLocation = dataTaken[inx].startLocation
var endLocation = dataTaken[inx].endLocation


var markerCampusCentre = new mapboxgl.Marker()
        .setLngLat(startLocation)
        .addTo(map);

let marker = new mapboxgl.Marker({
                color: "red"
            });
            marker.setLngLat(endLocation);
            marker.addTo(map);

            let popup = new mapboxgl.Popup({
                offset: 20
            });

            popup.setText("endLocation");
            marker.setPopup(popup);

            map.easeTo({
                center: endLocation ,
                zoom: 16
            }); 


map.on('load', function () {
        map.addLayer({
            "id": "route",
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [[startLocation[0],startLocation[1]],
                                        [endLocation[0],endLocation[1]]]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#888",
                "line-width": 8
            }
        })});
    







