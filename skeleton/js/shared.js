// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

// Run Class
// Creating Run class
class run {
	constructor(name,startLocation, endLocation, pathTaken, date, startTime, endTime,avgSpeed) {
		this._startLocation = startLocation;
		this._endLocation = endLocation;
		this._pathTaken = pathTaken;
		this._date = date;
		this._startTime = startTime;
        this._endTime = endTime;
        this._name = name;
        this._avgSpeed = avgSpeed;
	};
    get name() {
        return this.name;
    }
    get pathTaken() {
        return this.pathTaken;
    }
    get timeTaken() {
        return this.timeTaken;
    }
    get avgSpeed() {
        return this.avgSpeed;
    }
}



var runList= {}, currentPath=[]
function saveRun(){
    document.getElementById('startRun').setAttribute("disabled","")
    document.getElementById('resetRun').removeAttribute("disabled");
    
    
    if (localStorage.getItem('tempIndex') !== null){
        var dataTaken = JSON.parse(localStorage.getItem('runList'));
        var tempIndex = localStorage.getItem('tempIndex');
        var updateDate = new Date();
        dataTaken[tempIndex].countingTime = countingTime;
        
        localStorage.setItem('runList',JSON.stringify(dataTaken));
        localStorage.removeItem('tempIndex')
        
        
        }
        else{
    
    //Allow user to give short description to the run.
    runList.name = prompt("Please enter short description of this run", "Enter Name");
    runList.startLocation = mylocation;
    runList.endLocation = randompoint;
    runList.date = date;
    runList.time = countingTime;
    runList.avgSpeedRun = distance/countingTime;
    runList.distanceT = distance;
        
    

    //Save the past run to the local storage.
   currentPath.push(runList);
    if (localStorage.getItem('runList')=== null){
        localStorage.setItem('runList', JSON.stringify(currentPath));
    } else {
      var tempList=JSON.parse(localStorage.getItem('runList'));
      tempList.push(runList);
      localStorage.setItem('runList', JSON.stringify(tempList));
    }
    
    //Redirect the user to Index page.
   
}}

function reAttempt(){  
    localStorage.setItem("reAttempt", 'true');
}






