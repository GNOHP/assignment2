// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
var runList = JSON.parse(localStorage.getItem("runList"))
document.getElementById("1st").innerHTML = runList[0].name
document.getElementById("2nd").innerHTML = runList[1].name
document.getElementById("3rd").innerHTML = runList[2].name
document.getElementById("4th").innerHTML = runList[3].name
document.getElementById("5th").innerHTML = runList[4].name


function viewRun(i)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    var index = i;
    localStorage.setItem("index",index)
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}
