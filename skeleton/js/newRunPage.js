// Code for the Measure Run page.
     
//Code to show the current location on the map.   
        let Clayton = [145.1362585,-37.9128781];
    
        //Show the map on the run page.
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: Clayton
        });
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
            enableHighAccuracy: true
            },
        trackUserLocation: true 
        }));



    //Blue marker to pin the current location.
    var markerHeight = 50, markerRadius = 10, linearOffset = 25;
    var popupOffsets = {
             'top': [0, 0],
             'top-left': [0,0],
             'top-right': [0,0],
             'bottom': [0, -markerHeight],
             'bottom-left': [linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
             'bottom-right': [-linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
             'left': [markerRadius, (markerHeight - markerRadius) * -1],
             'right': [-markerRadius, (markerHeight - markerRadius) * -1]};



    //Disable 'START RUN' button when accuracy is more than 20.
    function success(pos){
        var crd=pos.coords;
        var accr=crd.accuracy;
        var newAcc=document.getElementById("accr");
        newAcc.innerHTML += accr

        if (accr<50){
        document.getElementById("generateRandomLocation").disabled=false;
        document.getElementById("startRun").disabled=false;
        document.getElementById("saveRun").disabled=false;
        }
    }
    navigator.geolocation.getCurrentPosition(success)

    //Get the latitude and longitude of the current position (starting point).
    var crd,Lat,Long,randompoint,currentLng,currentLat;
    function x(pos) {
            crd = pos.coords;
            currentLat = crd.latitude;
            currentLng = crd.longitude;
            console.log(currentLat);
            console.log(currentLng);
    }
    navigator.geolocation.getCurrentPosition(x);
    

    var mylocation,newDistance,lngDistance,latDistance,distance,randomLongitude,randomLatitude;
   


    function generateRandomLocation(){
        //localStorage.removeItem("runList")
        var zoomlevel = map.transform._zoom;
        
            //Disable the 'START RUN' button before a new random point is found.
            document.getElementById('startRun').removeAttribute("disabled");
        
        //If-else statement to prevent the user from zooming the map too big and not seeing anything.
        if(zoomlevel<20){
            //Put the 'my location' banner on the current location (starting point).
            mylocation = [currentLng,currentLat]
            var popupCampusCentre = new mapboxgl.Popup({
                offset: popupOffsets,
                className: 'my-class'})
                .setLngLat(mylocation)
                .setHTML("<h1> my location </h1>")
                .addTo(map);
            var markerCampusCentre = new mapboxgl.Marker()
                .setLngLat(mylocation)
                .addTo(map);
            
            //Find the new random point.
            var randomValue1 =(Math.random() * 0.0006 + 0.0004)*(Math.random() < 0.5 ? -1 : 1);
            var randomValue2 =(Math.random() * 0.0006 + 0.0004)*(Math.random() < 0.5 ? -1 : 1);
            var randomLongitude=currentLng+randomValue1;
            var randomLatitude=currentLat+randomValue2;
            randompoint = [randomLongitude,randomLatitude];
        

            //Red marker to pin the random point.
            let marker = new mapboxgl.Marker({
                color: "red"
            });
            marker.setLngLat(randompoint);
            marker.addTo(map);

            let popup = new mapboxgl.Popup({
                offset: 20
            });

            popup.setText("randompoint");
            marker.setPopup(popup);

            map.easeTo({
                center: randompoint ,
                zoom: 16
            }); 


            //Show line between current position (starting point) to random point.
            map.addLayer({
                "id": "route",
                "type": "line",
                "source": {
                    "type": "geojson",
                    "data": {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                        "type": "LineString",
                        "coordinates": [
                                [currentLng, currentLat],
                                [randompoint[0], randompoint[1]]
                        ]
                        }
                    }
                },
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 6
                    }
            });

            //Record distance from starting point to target point
            lngDistance=Math.abs(currentLng-randomLongitude);
            latDistance=Math.abs(currentLat-randomLatitude);
            distance = Math.sqrt(Math.pow(lngDistance,2)+Math.pow(latDistance,2))*120000;
            newDistance=document.getElementById("distance");
            newDistance.innerHTML += distance.toFixed(3) + 'm';

    
            map.easeTo({
                center: randompoint ,
                zoom: 16
            }); 

            map.easeTo({
                center: mylocation ,
                zoom: 16
            })

        } else{
            alert('You are zooming too big');
        }
    }



    var date,time    
    function startRun(){
        // Disable the 'GENERATE RANDOM LOCATION' button when the 'START RUN' button is clicked.
        document.getElementById('generateRandomLocation').setAttribute("disabled","")
        
        
        setInterval(timedCount(),1000);
        
        //Record date and time at start run.
        if(localStorage.getItem("tempIndex")===null){
        var startRunTime = new Date();
        date = startRunTime.getFullYear()+'-'+(startRunTime.getMonth()+1)+'-'+startRunTime.getDate();
        time = startRunTime.getHours() + ":" + startRunTime.getMinutes() + ":" + startRunTime.getSeconds();
        var startDateTime = date+' '+time;}
        
        setInterval(distanceTravelled(),1000);
        setInterval(navigator.geolocation.getCurrentPosition(x))

        
    

        
        var id, target, options;
        
        //Watch the position of the user everytime it is moving.
        function success() {
            if ((target.latitude === mylocation.lat+0.000075||target.latitude === mylocation.lat-0.000075) && (target.longitude === mylocation.lng+0.000075||target.longitude === mylocation.lng-0.000075)) { 
                alert('Congratulations, you reached the target!');
                var completeRunTime = new Date();
                var date = completeRunTime.getFullYear()+'-'+(completeRunTime.getMonth()+1)+'-'+completeRunTime.getDate();
                var time = completeRunTime.getHours() + ":" + completeRunTime.getMinutes() + ":" + completeRunTime.getSeconds();          
                var completeDateTime = date+' '+time;
                console.log(completeDateTime);
                navigator.geolocation.clearWatch(id);
              
            } 
        

      function error(err) {
            console.warn('ERROR(' + err.code + '): ' + err.message); 
        }

       

        options = { 
        enableHighAccuracy: true,
        timeout: 5000, 
        maximumAge: 0 
        }; 

        id = navigator.geolocation.watchPosition(success, error, options);

        //Call the function every one second (1000 miliseconds).
        
        
        }}
    

    function resetRun(){
          let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: Clayton
        });
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
            enableHighAccuracy: true
            },
        trackUserLocation: true 
        }));
        
        mylocation = [];
        randompoint = [];
        distance =[];
        date=[];
        time=[];
        setInterval(clearAll(),1000);
        //Enable and disable buttons once the user wants to reset run.
        document.getElementById('generateRandomLocation').removeAttribute("disabled");
        document.getElementById('saveRun').setAttribute("disabled","")
        document.getElementById('resetRun').setAttribute("disabled","")
    }



    //Find the distance travelled by calculating the distance between the starting point and the user's current location.
    function distanceTravelled(){
        var lng=Math.abs(currentLng-randompoint[0]);
        var lat=Math.abs(currentLat-randompoint[1]);
        var distanceLeft = Math.sqrt(Math.pow(lng,2)+Math.pow(lat,2))*120000;
        
        if (distanceLeft < 15){
            document.getElementById('saveRun').removeAttribute("disabled");
        }
        
        var distanceTravelled=distance-distanceLeft;
        DistanceTravelled=document.getElementById("distanceTravelled");
        DistanceTravelled.innerHTML += distanceTravelled.toFixed(3) +'m';
    }



    var countingTime=0;
    var time;
    function timedCount() {
        var temptextmin=document.getElementById('txt');
        hour = parseInt(countingTime / 3600);// hour
        min = parseInt(countingTime / 60);// min
        if(min>=60){
	       min=min%60
        }
        lastsecs = countingTime % 60;


        temptextmin.value = hour + "h" + min + "m" + lastsecs + "s"

        countingTime = countingTime+1
        time=setTimeout("timedCount()",1000)
    }



    function stopCount() {
        clearTimeout(time)
        document.getElementById('start').style.display = "";   
        document.getElementById('end').style.display = "none"; 
    }


    function clearAll(){
        countingTime=0
        document.getElementById('txt').value=  "0h" +  "0m" + "0s"
        clearTimeout(time)
        document.getElementById('start').style.display ="";   
        document.getElementById('end').style.display = "none"; 
    }


if (localStorage.getItem('reAttempt') !== null){
    document.getElementById('startRun').removeAttribute("disabled");
    document.getElementById('generateRandomLocation').setAttribute("disabled","")
    
    var momentIndex = localStorage.getItem('index');
    localStorage.setItem('tempIndex',momentIndex);
    var inList = JSON.parse(localStorage.getItem('runList'))[momentIndex];
    
    map.panTo(inList.startLocation);
    
    var marker = new mapboxgl.Marker({
                color: "red"
            });
            marker.setLngLat(inList.endLocation);
            marker.addTo(map);

            let popup = new mapboxgl.Popup({
                offset: 20
            });

            popup.setText("endLocation");
            marker.setPopup(popup);

            map.easeTo({
                center: inList.endLocation ,
                zoom: 16
            }); 
    
var startLocation = new mapboxgl.Marker()
        .setLngLat(inList.startLocation)
        .addTo(map);
    
    
    localStorage.removeItem('reAttempt')
    
    
    
}








